<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->AddHeadString('<link href="'.$APPLICATION->GetCurDir().'"style.css";  type="text/css" rel="stylesheet" />',true);
$APPLICATION->AddHeadScript($this->GetFolder() . '/script.js');
?>

<script>
  window.onload = function() {
    changedSearchSelector();
  };
</script>


    <table>
        <tr>
            <td colspan="8">
                <div class="submitBtn"><input type="file" onchange="fileUpload(this.files); " /></div>
            </td>
        </tr>
        <tr>
        <form action="" method="POST">
            <td>
                <div class="filterCaption">Кол-во комнат:</div>
            </td>
            <td class="filterSelector">
                <select id="roomSelector" name="room" onchange="changedSearchSelector(); ">
                    <option value="">Все</option>
                    <? foreach ($arResult['UNIQUE_ROOMS'] as $room) {
                        print("<option value=" . $room . ">" . $room . "</option>");
                    } ?>
                </select>
            </td>
            <td>
                <div class="filterCaption">Проект:</div>
            </td>
            <td class="filterSelector">
                <select id="projectSelector" name="project" onchange="changedSearchSelector(); ">
                    <option value="">Все</option>
                    <? foreach ($arResult['UNIQUE_PROJECTS'] as $project) {
                        print("<option value=" . $project . ">" . $project . "</option>");
                    } ?>
                </select>
            </td>
            <td>
                <div class="filterCaption">Площадь, м<sup>2</sup>:</div>
            </td>
            <td class="filterSelector">
                <select id="squareSelector" name="square" onchange="changedSearchSelector(); ">
                    <option value="">Все</option>
                    <? foreach ($arResult['UNIQUE_SQUARES'] as $square) {
                        print("<option value=" . $square . ">" . $square . "</option>");
                    } ?>
                </select>
            </td>
            <td>
                <div class="filterCaption">Найдено: </div>
            </td>
            <td>
                <div id="countResultOutput"></div>
            </td>
        </form>
        </tr>
    </table>


<br>
<div id="resultOutput"></div>
