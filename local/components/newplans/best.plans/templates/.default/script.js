function changedSearchSelector() {
    var roomSelector = document.getElementById("roomSelector");
    var roomSelectorValue = roomSelector.options[roomSelector.selectedIndex].value;
    var projectSelector = document.getElementById("projectSelector");
    var projectSelectorValue = projectSelector.options[projectSelector.selectedIndex].value;
    var squareSelector = document.getElementById("squareSelector");
    var squareSelectorValue = squareSelector.options[squareSelector.selectedIndex].value;
    
    $.ajax({
        url: "/local/components/newplans/best.plans/list.ajax.php",
        type: "POST",
        data: {
            room    : roomSelectorValue,
            project : projectSelectorValue,
            square  : squareSelectorValue
        },
        dataType: "json",
        success: function (data) {
            var htmlOutput = '<table><tr>';
            data.forEach(function(item, i, arr) {
                htmlOutput += '<td>';
                htmlOutput += '<table>';
                htmlOutput += "<tr><td><div><img src='" + item.URL_FOR_DOWNLOAD + "' width='100' height='100'></div></td></tr>";
                htmlOutput += "<tr><td><div>";
                htmlOutput += item.FOLDER_ROOM_NAME+"<br>"+item.FOLDER_PROJECT_NAME+"<br>"+item.FOLDER_SQUARE_NAME;
                htmlOutput += "</div></td></tr>";
                htmlOutput += '</table>';
                htmlOutput += '</td>';
            });
            htmlOutput += '</tr></table>';
            $("#resultOutput").html(htmlOutput);
            $("#countResultOutput").html(data.length);
        }
    });
}


function fileUpload(selFile) {

    var fd = new FormData;
    fd.append('img', selFile[0]);
    
    $.ajax({
        url: "/local/components/newplans/best.plans/addfile.ajax.php",
        type: "POST",
        data: fd,
        contentType: false,
        processData: false,

        success: function (data) {
            console.log(data);
        }
    });
//    console.log(fd);
}

