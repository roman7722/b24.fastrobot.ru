<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\Loader,
    \Bitrix\Disk;

function readPlansFolder($selectedRoom = "", $selectedProject = "", $selectedSquare = "") {
    try
    {
        if ( !Loader::includeModule('disk') ) {
            throw new Exception("Не подклчюен модуль диска");
        }
        $driver = Disk\Driver::getInstance();
        $securityContext = $driver->getFakeSecurityContext();
        $iFolderId = 554;
        $folder = Disk\Folder::loadById($iFolderId);
        $arPlanFolderList = [];

        getRecurciveFolder($folder, $securityContext, $arPlanFolderList, $driver);

        $arFolderList = $arPlanFolderList[0]["CHILDRENS"];
        $arResult['FOLDERLIST'] = $arFolderList;

        $arAllRooms = [];
        $arRoomsUnique = [];
        $arProjectsUnique = [];
        $arSquareUnique = [];

        foreach ($arFolderList as $rooms) {
            $arRoomsUnique[$rooms["NAME"]] = NULL;
            foreach($rooms["CHILDRENS"] as $projects) {
                $arProjectsUnique[$projects["NAME"]] = NULL;
                foreach($projects["CHILDRENS"] as $squares) {
                    $arSquareUnique[$squares["NAME"]] = NULL;
                    foreach ($squares["CHILDRENS"] as $plan) {
                        $arAllRooms[] = [
    //                        'FOLDER_ROOM_ID'        => $rooms["ID"],        
                            'FOLDER_ROOM_NAME'      => $rooms["NAME"],
    //                        'FOLDER_PROJECT_ID'     => $projects["ID"],     
                            'FOLDER_PROJECT_NAME'   => $projects["NAME"],
    //                        'FOLDER_SQUARE_ID'      => $squares["ID"],
                            'FOLDER_SQUARE_NAME'    => $squares["NAME"],
                            'URL_FOR_DOWNLOAD'      => $plan["URLFORDOWNLOAD"],
                        ];
                    }
                }
            }
        }

        $arResult['ALL_ROOMS']          = $arAllRooms;
        $arResult['UNIQUE_ROOMS']       = array_keys($arRoomsUnique);
        $arResult['UNIQUE_PROJECTS']    = array_keys($arProjectsUnique);
        $arResult['UNIQUE_SQUARES']     = array_keys($arSquareUnique);
        natsort($arResult['UNIQUE_SQUARES']);
        $arResult['UNIQUE_SQUARES'] = array_map(function($el) { return strval($el); }, $arResult['UNIQUE_SQUARES']);
               
        $arResult['SELECTED_ROOMS'] = [];
        foreach ($arResult['ALL_ROOMS'] as $someRoom) {
            if (empty($selectedRoom)) { $room_name = ""; } else { $room_name = $someRoom['FOLDER_ROOM_NAME']; }
            if (empty($selectedProject)) { $project_name = ""; } else { $project_name = $someRoom['FOLDER_PROJECT_NAME']; }
            if (empty($selectedSquare)) { $square_name = ""; } else { $square_name = $someRoom['FOLDER_SQUARE_NAME']; }
            if ((strcmp($room_name, $selectedRoom) === 0) && 
                (strcmp($project_name, $selectedProject) === 0) && 
                (strcmp($square_name, $selectedSquare) === 0)) {
                    $arResult['SELECTED_ROOMS'][] = $someRoom;
            }
        }
    }
    catch( Exception $e )
    {
       echo "Произошла ошибка: \r\n";
       var_dump($e->getMessage());
    }
    
    return $arResult;
}

function getRecurciveFolder($diskObject, $securityContext, &$arPlanFolderList, $driver) {
    
    if ( $diskObject instanceof Disk\Folder ) {
        $arFolder = [
            'ID'        => $diskObject->getId(),
            'NAME'      => $diskObject->getName(),
            'IS_FOLDER' => true,
            'CHILDRENS' => [],
        ];

        $arChildrens = $diskObject->getChildren($securityContext);

        foreach ($arChildrens as $childObject) {
            getRecurciveFolder($childObject, $securityContext, $arFolder['CHILDRENS'], $driver);
        }

        $arPlanFolderList[] = $arFolder;
    } else {
        $arPlanFolderList[] = [
            'ID'                => $diskObject->getId(),
            'NAME'              => $diskObject->getName(),
            'IS_FOLDER'         => false,
            'URLFORDOWNLOAD'    => $driver->getUrlManager()->getUrlForDownloadFile($diskObject, true),
//            'URLFORDOWNLOAD'    => CFile::GetPath($diskObject->getId()),
        ];
    }
}

?>
