<?
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");
require_once('functions.php');

if(isset($_POST['room'])) {
    $selectedRoom = $_POST['room'];
}
if(isset($_POST['project'])) {
    $selectedProject = $_POST['project'];
}
if(isset($_POST['square'])) {
    $selectedSquare = $_POST['square'];
}

$arResult = readPlansFolder($selectedRoom, $selectedProject, $selectedSquare);

$data = $arResult['SELECTED_ROOMS'];

echo json_encode($data);

?>
