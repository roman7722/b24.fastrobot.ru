<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "Новые планировки",
    "DESCRIPTION" => "Выводим новые планировки",
    "ICON" => "/images/plans.gif",
    "PATH" => array(
        "ID" => "newplan_components",
        "CHILD" => array(
            "ID" => "plans",
            "NAME" => "Планировки"
        )
    ),
);

?>